import json
from time import sleep

from bs4 import BeautifulSoup
from kafka import KafkaConsumer, KafkaProducer


def publish_message(producer_instance, topic_name, key, value):
    try:
        key_bytes = bytes(key, encoding='utf-8')
        value_bytes = bytes(value, encoding='utf-8')
        producer_instance.send(topic_name, key=key_bytes, value=value_bytes)
        producer_instance.flush()
        print('Done')
    except Exception as ex:
        print('Publishing error')
        print(str(ex))


def connect_kafka_producer():
    _producer = None
    try:
        _producer = KafkaProducer(bootstrap_servers=['localhost:9092'], api_version=(0, 10))
    except Exception as ex:
        print('Connection error')
        print(str(ex))
    finally:
        return _producer


def parse(markup):
    title = '-'
    submit_by = '-'
    occupation = '-'
    age = 0
    skills = []
    rec = {}

    try:
        soup = BeautifulSoup(markup, 'lxml')
        # Name
        name_section = soup.select('.name__h1')
        # Surname
        surname_section = soup.select('.surname')
        # Occupation    
        occupation_section = soup.select('.occupation')
        # Skills
        skills_section = soup.select('.skills_txt')
        # Age
        age_section = soup.select('.age')
        if age_section:
            age = age_section[0].text.replace('age', '').strip()

        if skills_section:
            for skill in skills_section:
                skill_text = skill.text.strip()
                if 'Add all skills to list' not in skill_text and skill_text != '':
                    skills.append({'step': skill.text.strip()})

        if occupation_section:
            occupation = occupation_section[0].text.strip().replace('"', '')

        if surname_section:
            submit_by = surname_section[0].text.strip()

        if name_section:
            title = name_section[0].text

        rec = {'title': title, 'surname': submit_by, 'occupation': occupation, 'age': age,
               'skills': skills}

    except Exception as ex:
        print('Parsing error')
        print(str(ex))
    finally:
        return json.dumps(rec)


if __name__ == '__main__':
    print('Running Consumer..')
    parsed_records = []
    topic_name = 'raw_bio'
    parsed_topic_name = 'parsed_bio'

    consumer = KafkaConsumer(topic_name, auto_offset_reset='earliest',
                             bootstrap_servers=['localhost:9092'], api_version=(0, 10), consumer_timeout_ms=1000)
    for msg in consumer:
        html = msg.value
        result = parse(html)
        parsed_records.append(result)
    consumer.close()
    sleep(5)

    if len(parsed_records) > 0:
        print('Publishing...')
        producer = connect_kafka_producer()
        for rec in parsed_records:
            publish_message(producer, parsed_topic_name, 'parsed', rec)
