import json
from time import sleep

from kafka import KafkaConsumer

if __name__ == '__main__':
    parsed_topic_name = 'parsed_bio'
    age_threshold = 200

    consumer = KafkaConsumer(parsed_topic_name, auto_offset_reset='earliest',
                             bootstrap_servers=['localhost:9092'], api_version=(0, 10), consumer_timeout_ms=1000)
    for msg in consumer:
        record = json.loads(msg.value)
        age = int(record['age'])
        title = record['name']

        if age > age_threshold:
            print('Alert: {} age count is {}'.format(title, age))
        sleep(3)

    if consumer is not None:
        consumer.close()